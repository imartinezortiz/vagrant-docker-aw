#!/usr/bin/env bash
set -euo pipefail
[[ "${DEBUG:-false}" == "true" ]] && set -x

mv /home/vagrant/docker-compose@.service /etc/systemd/system
# Update configuration if the provisioner is run more than once
systemctl daemon-reload

mkdir -p /var/lib/docker-compose/aw-sw
mkdir -p /var/lib/docker-compose/aw-sw/apache2
mkdir -p /var/lib/docker-compose/aw-sw/log
mkdir -p /var/lib/docker-compose/aw-sw/php

curl -fsSL -o /var/lib/docker-compose/docker-compose.yml https://raw.githubusercontent.com/informaticaucm/docker-aw-sw/master/docker-compose.yml

sed -i -r \
  -e 's|\./servidor/apache2|\./apache2|g' \
  -e 's|\./servidor/php|\./php|g' \
  -e 's|\./servidor/log|\./log|g' \
  -e 's|\./servidor/www|/vagrant/|g' \
  /var/lib/docker-compose/docker-compose.yml

systemctl enable docker-compose@aw-sw
systemctl start docker-compose@aw-sw