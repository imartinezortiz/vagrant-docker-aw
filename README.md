# Infraestructura para las prácticas de las asignaturas relacionadas con las Aplicaciones Web

## Creación de entorno de pruebas para AW / SW

El entorno de pruebas que utilizamos en AW es un entorno creado con la herramienta [Docker](https://www.docker.com), en particular hemos publicado el [contenedor para AW](https://hub.docker.com/r/informaticaucm/docker-aw-sw) en el respositorio Docker Hub.

Como paso previo, tienes que asegurarte que tienes instalada/actualizada la herramienta [Virtualbox](http://virtualbox.org).

Para utilizar Docker en tu equipo tienes las siguientes opciones:

1. Modo avanzado:  [Vagrant](https://www.vagrantup.com/). Si te apetece cacharrear y controlar al detalle que sucede en tu máquina, puedes instalar la herramienta Vagrant y utilizar este repositorio para crear una VM Ubuntu 18.04 con Docker instalado dentro de tu VirtualBox que te servirá como anfitrión.

2. Modo experto: Te instalas a mano una VM Ubuntu >= 18.04, instalas Docker y ya tendrás un anfitrión de contenedores listo.

Pese a que cada vez está mas pulido, el modo básico puede dar problemas en tu equipo y el modo experto requiere realizar una instalación manual y completa de una VM linux, por lo que la opción recomendada es la opción intermedia.

### Anfitrión con Vagrant

Una vez instalados Virtualbox, Vagrant y git (opcionalmente), tienes que:

1. Descargar este repositorio:

   1. Desde la interfaz web de bitbucket.

   2. Utilizando git o un frontend para git.

2. Abrir un terminal (cmd, PowerShell, bash, etc.) e ir al directorio donde tienes el repositorio.

3. Crear el anfitrión de contenedores con `vagrant up`.

4. Esperar hasta que termine de aprovisionar la máquina.

> NOTA: El aprovisionamiento puede tardar bastante tiempo ya que es necesario descargar la imagen de la VM (~300MB) y las imágenes de docker utilizadas (~1.2GB).

Una vez se ha aprovisionada la máquina, puedes acceder a los diferente servicios en los siguientes puertos:

* Puerto `8080`: Apache
* Puerto `8081`: phpMyAdmin
* Puerto `8022`: SSH

## Anexo: Creación de una pila de desarrollo para AW / SW dentro del anfitrión

El despliegue de la infraestructura de prácticas se encuentra automatizada en 2 scripts:

1. `scripts/bootstrap-docker.sh`: Automatiza la instalación de docker y docker-compose.
2. `scripts/bootstrap-containers.sh`: Automatiza el uso de systemd para gestionar proyectos gestionados con docker-compose e instala una unidad de systemd basada en la configuración proporcionada en el repositorio [informaticaucm/docker-aw-sw](https://github.com/informaticaucm/docker-aw-sw).

```
┌───────────────────────────────────────────────────────┐
│                                                       │
│              ┌────────────────────────────────────┐   │
│     8080 ────│─────\       ┌───────────────────┐  │   │
│    (http)    │      \ 8080 │                   │  │   │
│              │       \─────│                   │  │   │
│     8081 ────│─────\       │                   │  │   │
│ (phpmyadmin) │      \ 8081 │   /var/www─────────┐ │   │
│              │       \─────│                   ││ │   │
│     8022 ────│─────\       │                   ││ │   │
│     (ssh)    │      \ 8022 │                   ││ │   │
│              │       \─────│                   ││ │   │
│              │             └───────────────────┘│ │   │
│              │             Contenedor AW        │ │   │
│              │                                  │ │   │
│              │               /vagrant<──────────┘ │   │
│              │                  └───────────────────┐ │
│              └────────────────────────────────────┘ │ │
│            VM anfitrión de contenedores Docker      │ │
│                                                     │ │
│   C:\Users\Usuario\vagrant-docker-aw <──────────────┘ │
│   ó /home/Usuario/vagrant-docker-aw                   │
└───────────────────────────────────────────────────────┘
   Tu sistema operativo
```